module util

use math, only: PI

implicit none

contains

! subroutine check_waypoint(Y_wp, Y, var, bool, tol)
!     real(8),intent(in)  :: Y_wp(:), Y(:), tol
!     integer,intent(in)  :: var
!     logical,intent(out) :: bool
!     if (abs((Y(var)-Y_wp(var))/Y_wp(var)) < tol) then
!         bool = .TRUE.
!         ! write(*,*) bool
!     end if
! end subroutine check_waypoint

subroutine arregla_angulo (ang)
    real(8), intent(inout) :: ang
    if (ang > 0d0 .and. ang > 2d0*PI) then
        do while (ang > 2d0*PI)
            ang = ang - 2d0*PI
        end do
    else if (ang < 0d0 .and. ang < 2d0*PI) then
        do while (ang < 2d0*PI)
            ang = ang + 2d0*PI
        end do
    end if
end subroutine arregla_angulo

subroutine comprobar_diana (Y, Y_min, Y_max, DENTRO)
    real(8), dimension(4), intent(in) :: Y, Y_min, Y_max
    logical, intent(out) :: DENTRO
    
    integer :: i
    integer :: bool_aux(4)
    
    DENTRO = .FALSE.
    
    bool_aux(:) = 0
    
    do i = 1,4
        if (Y(i) > Y_min(i) .and. Y(i) < Y_max(i)) then
            bool_aux(i) = 1
        end if
!         !!!y luego comprobar que todo bool_aux sea true --> dentro
    end do
    
    if (sum(bool_aux) == 4) then
        DENTRO = .TRUE.
    else
        DENTRO = .FALSE.
    end if

!     do i = 1,4
        !!print*, i
!         if (Y(i) < Y_min(i) .or. Y(i) > Y_max(i)) then
!             DENTRO = .FALSE.
            !!PRINT*, i,dentro
!         end if
!     end do

end subroutine comprobar_diana

subroutine check_waypoint(Y_wp, Y, var, bool)
    real(8),intent(in)  :: Y_wp(:), Y(:)
    integer,intent(in)  :: var
    logical,intent(out) :: bool
    if (Y(var)-Y_wp(var) < 0d0) then
        bool = .TRUE.
        ! write(*,*) bool
    end if
end subroutine check_waypoint

subroutine separarY (Y, v, th, h, x)
    real(8), intent(in) :: Y(4)
    real(8), intent(out):: v, th, h, x
    
    v  = Y(1)
    th = Y(2)
    h  = Y(3)
    x  = Y(4)
    
end subroutine separarY

subroutine separarU (U, T, d)
    real(8), intent(in) :: U(2)
    real(8), intent(out):: T, d
    
    T = U(1)
    d = U(2)

end subroutine separarU

FUNCTION Y2X(Y) result(X)
    real(8), intent(in) :: Y(4)
    real(8) :: X(2)
    
    X(1) = Y(1) ! v
    X(2) = Y(2) ! th

END FUNCTION

end module util
