module guiado

use math, only: linspace, pol2space, polnspace, cubica, PI
use linalg, only: m44inv
use vehiculo
use tierra, only: atmosisa_rho, modelo_gravedad
use util, only: separarY, separarU

implicit none

contains

subroutine crear_WP_1tramo (Y1, Y2, npuntos, waypoints)
    real(8),intent(in)  :: Y1(4), Y2(4)
    integer,intent(in)  :: npuntos
    real(8),intent(inout) :: waypoints(size(Y1),npuntos)
    
    real(8) :: th1,th2,h1,h2,x1,x2,m(4,4),b(4)
    logical :: ok
    integer :: i
    
    waypoints(:,1) = Y1
    call pol2space(Y1(1),Y2(1),npuntos,waypoints(1,:npuntos))
    call pol2space(Y1(3),Y2(3),npuntos,waypoints(3,:npuntos))

    th1 = Y1(2)
    h1  = Y1(3)
    x1  = Y1(4)
    th2 = Y2(2)    
    h2  = Y2(3)
    x2  = Y2(4)

    m(1,1) = h1**3
    m(1,2) = h1**2
    m(1,3) = h1
    m(1,4) = 1d0
    m(2,1) = h2**3
    m(2,2) = h2**2
    m(2,3) = h2
    m(2,4) = 1d0
    m(3,1) = 3d0*h1**2
    m(3,2) = 2d0*h1
    m(3,3) = 1d0
    m(3,4) = 0d0
    m(4,1) = 3d0*h2**2
    m(4,2) = 2d0*h2
    m(4,3) = 1d0
    m(4,4) = 0d0

    b(1) = x1
    b(2) = x2
    b(3) = dtan(th1)
    b(4) = dtan(th2)
    
    call m44inv(m,m,ok)
    b = MATMUL(m,b)

    do i = 1,npuntos
        waypoints(4,i) = cubica(waypoints(3,i),b)
    enddo

    do i = 1, npuntos-1
        waypoints(2,i) = datan2(-(waypoints(4,i+1)-waypoints(4,i)),-(waypoints(3,i+1)-waypoints(3,i)))
    enddo

end subroutine

subroutine crear_waypoints (Yi,Yf,Yc,npuntos,waypoints)
    real(8),intent(in)  :: Yi(4), Yf(4), Yc(4)
    integer,intent(in)  :: npuntos(2)
!     real(8),allocatable,intent(out) :: waypoints(:,:)
    real(8),intent(inout) :: waypoints(size(Yi),npuntos(1)+npuntos(2))
    
    integer :: i
    real(8) :: th1,th2,h1,h2,x1,x2,m(4,4),b(4)
    LOGICAL :: ok

!     allocate(waypoints(size(Yi),npuntos(1)+npuntos(2)))

    waypoints(:,1) = Yi
    call pol2space(Yi(1),Yc(1),npuntos(1),waypoints(1,:npuntos(1)))
    call pol2space(Yi(3),Yc(3),npuntos(1),waypoints(3,:npuntos(1)))

    th1 = Yi(2)
    h1  = Yi(3)
    x1  = Yi(4)
    th2 = Yc(2)    
    h2  = Yc(3)
    x2  = Yc(4)

    m(1,1) = h1**3
    m(1,2) = h1**2
    m(1,3) = h1
    m(1,4) = 1d0
    m(2,1) = h2**3
    m(2,2) = h2**2
    m(2,3) = h2
    m(2,4) = 1d0
    m(3,1) = 3d0*h1**2
    m(3,2) = 2d0*h1
    m(3,3) = 1d0
    m(3,4) = 0d0
    m(4,1) = 3d0*h2**2
    m(4,2) = 2d0*h2
    m(4,3) = 1d0
    m(4,4) = 0d0

    b(1) = x1
    b(2) = x2
    b(3) = dtan(th1)
    b(4) = dtan(th2)

    ! do i = 1,4
    !     write(*,*) m(i,:)
    ! enddo
    ! write(*,*) b

    call m44inv(m,m,ok)
    ! do i = 1,4
        ! write(*,*) m(i,:)
    ! enddo
    b = MATMUL(m,b)
    ! write(*,*) b

    do i = 1,npuntos(1)
        waypoints(4,i) = cubica(waypoints(3,i),b)
    enddo
    ! do i = 2, npuntos(1)
        ! waypoints(2,i) = datan2(-(waypoints(4,i)-waypoints(4,i-1)),-(waypoints(3,i)-waypoints(3,i-1)))
    ! enddo
    do i = 1, npuntos(1)-1
        waypoints(2,i) = datan2(-(waypoints(4,i+1)-waypoints(4,i)),-(waypoints(3,i+1)-waypoints(3,i)))
    enddo

    ! waypoints(:,npuntos(1)) = Yc
    call pol2space( Yc(1), Yf(1), npuntos(2)+1, waypoints(1,npuntos(1):npuntos(1)+npuntos(2)) )
    call pol2space( Yc(3), Yf(3), npuntos(2)+1, waypoints(3,npuntos(1):npuntos(1)+npuntos(2)) )

    th1 = Yc(2)
    h1  = Yc(3)
    x1  = Yc(4)
    th2 = Yf(2)    
    h2  = Yf(3)
    x2  = Yf(4)

    m(1,1) = h1**3
    m(1,2) = h1**2
    m(1,3) = h1
    m(1,4) = 1d0
    m(2,1) = h2**3
    m(2,2) = h2**2
    m(2,3) = h2
    m(2,4) = 1d0
    m(3,1) = 3d0*h1**2
    m(3,2) = 2d0*h1
    m(3,3) = 1d0
    m(3,4) = 0d0
    m(4,1) = 3d0*h2**2
    m(4,2) = 2d0*h2
    m(4,3) = 1d0
    m(4,4) = 0d0

    b(1) = x1
    b(2) = x2
    b(3) = dtan(th1)
    b(4) = dtan(th2)
    
    ! do i = 1,4
        ! write(*,*) "***", m(i,:)
    ! enddo
    ! write(*,*) b

    call m44inv(m,m,ok)
    ! write(*,*) ok
    b = MATMUL(m,b)

    ! write(*,*) b

    do i = npuntos(1),npuntos(1)+npuntos(2)
        waypoints(4,i) = cubica(waypoints(3,i),b)
    enddo
    ! do i = npuntos(1)+1, npuntos(1)+npuntos(2)
        ! waypoints(2,i) = datan2(-(waypoints(4,i)-waypoints(4,i-1)),-(waypoints(3,i)-waypoints(3,i-1)))
    ! enddo
    do i = npuntos(1), npuntos(1)+npuntos(2)-1
        waypoints(2,i) = datan2(-(waypoints(4,i+1)-waypoints(4,i)),-(waypoints(3,i+1)-waypoints(3,i)))
    enddo
    waypoints(2,npuntos(1)+npuntos(2)) = Yf(2)

end subroutine crear_waypoints

subroutine calcularControlGuiadoVHL (Yg,Ug,VHLcond)
    real(8),intent(in) :: Yg(4), VHLcond(6)
    real(8),intent(out):: Ug(2)
    
    real(8) :: v_g, th_g, h_g, x_g
    real(8) :: T_g, d_g
    real(8) :: g, rho, W, S, C_B
    real(8) :: v_E, v_S, h_E, h_S, x_E, x_S
    real(8) :: h_M
    real(8) :: d_th_g_dh, d_th_g_dt, d_v_g_dh
    real(8) :: c_th_g, s_th_g, c_d_g, s_d_g
    
    call separarY(Yg, v_g, th_g, h_g, x_g)
    
    v_E = VHLcond(1)
    v_S = VHLcond(2)
    h_E = VHLcond(3)
    h_S = VHLcond(4)
    x_E = VHLcond(5)
    x_S = VHLcond(6)
    
    h_M = (h_E + h_S) / 2d0

    c_th_g = dcos(th_g)
    s_th_g = dsin(th_g)
    
    d_th_g_dh = 0d0
    d_th_g_dt = 0d0
    d_v_g_dh = (v_S - v_E)/(h_S - h_E)

    call atmosisa_rho   (h_M,rho)
    call modelo_gravedad(h_M,g)

    W = masa_primera_etapa * g
    S = PI * (DIAM_LANZADOR/2d0)**2
    C_B = rho * g * S * C_D / (2d0 * W)
    
    T_g = c_th_g - C_B*(v_g**2)/g + d_v_g_dh*v_g*c_th_g/g
    T_g = s_th_g**2 + T_g**2
    T_g = dsqrt(T_g)*W
    
    c_d_g = 1d0 + (-C_B*(v_g**2)*c_th_g + d_v_g_dh*v_g*(c_th_g)**2)/g
    c_d_g = c_d_g * W / T_g
    s_d_g = ( - C_B*(v_g**2)*s_th_g + d_v_g_dh*v_g*c_th_g*s_th_g)/g
    s_d_g = s_d_g * W / T_g
    d_g = datan2(s_d_g,c_d_g)
    
    Ug(1) = T_g
    Ug(2) = d_g

end subroutine calcularControlGuiadoVHL

subroutine calcularMatrizA (Yg, Ug, COND, A)
    real(8),intent(in) :: Yg(4), Ug(2), COND(6)
    real(8),intent(out):: A(2,2)
    
    real(8) :: v_g, th_g, h_g, x_g, T_g, d_g
    real(8) :: v_E, v_S, h_E, h_S, x_E, x_S
    real(8) :: h_M
    real(8) :: g, rho, W, S, C_B
    real(8) :: d_th_g_dh, d_th_g_dt, d_v_g_dh, d_v_g_dt
    real(8) :: c_th_g, s_th_g

    call separarY(Yg, v_g, th_g, h_g, x_g)
    call separarU(Ug, T_g, d_g)
    
    v_E = COND(1)
    v_S = COND(2)
    h_E = COND(3)
    h_S = COND(4)
    x_E = COND(5)
    x_S = COND(6)
    
    h_M = (h_E + h_S) / 2d0

    c_th_g = dcos(th_g)
    s_th_g = dsin(th_g)
    
    d_th_g_dh = 0d0
    d_th_g_dt = 0d0
    d_v_g_dh = (v_S - v_E)/(h_S - h_E)
    d_v_g_dt = d_v_g_dh * (-v_g) * c_th_g

    call atmosisa_rho   (h_M,rho)
    call modelo_gravedad(h_M,g)

    W = masa_primera_etapa * g
    S = PI * (DIAM_LANZADOR/2d0)**2
    C_B = rho * g * S * C_D / (2d0 * W)
    
    A(1,1) = - 2d0 * C_B * v_g
    A(1,2) = - d_th_g_dt * v_g
    A(2,1) = - d_th_g_dt / v_g
    A(2,2) = + C_B * v_g + d_v_g_dt / v_g
    
end subroutine calcularMatrizA

subroutine calcularMatrizB (Yg, Ug, COND, B)
    real(8),intent(in) :: Yg(4), Ug(2), COND(6)
    real(8),intent(out):: B(2,2)
    
    real(8) :: v_g, th_g, h_g, x_g, T_g, d_g
    real(8) :: v_E, v_S, h_E, h_S, x_E, x_S
    real(8) :: h_M
    real(8) :: g, rho, W!, S, C_B
!     real(8) :: d_th_g_dh, d_th_g_dt, d_v_g_dh, d_v_g_dt
!     real(8) :: c_th_g, s_th_g

    call separarY(Yg, v_g, th_g, h_g, x_g)
    call separarU(Ug, T_g, d_g)
    
    v_E = COND(1)
    v_S = COND(2)
    h_E = COND(3)
    h_S = COND(4)
    x_E = COND(5)
    x_S = COND(6)
    
    h_M = (h_E + h_S) / 2d0

!     c_th_g = dcos(th_g)
!     s_th_g = dsin(th_g)
    
!     d_th_g_dh = 0d0
!     d_th_g_dt = 0d0
!     d_v_g_dh = (v_S - v_E)/(h_S - h_E)
!     d_v_g_dt = d_v_g_dh * (-v_g) * c_th_g

!     call atmosisa_rho   (h_M,rho)
    call modelo_gravedad(h_M,g)

    W = masa_primera_etapa * g
!     S = PI * (DIAM_LANZADOR/2d0)**2
!     C_B = rho * g * S * C_D / (2d0 * W)
    
    B(1,1) = - g / W * dcos(th_g-d_g)
    B(1,2) = - g / W * dsin(th_g-d_g) * T_g
    B(2,1) = - g / W * dsin(th_g-d_g) / v_g
    B(2,2) = + g / W * dcos(th_g-d_g) * T_g / v_g

end subroutine calcularMatrizB

function derivadaGuiadoVHL(t,Y,RPAR)
    real(8), intent(in) :: t, Y(:), RPAR(:)
    ! integer, intent(inout) :: IPAR
    real(8) :: derivadaGuiadoVHL(size(Y))

    real(8) :: v_E, v_S, h_E, h_S, x_E, x_S
!     real(8) :: salto_alturas, salto_poshtal, th_g
    real(8) :: d_th_g_dh, d_th_g_dt, d_v_g_dh

    v_E = RPAR(1)
    v_S = RPAR(2)
    h_E = RPAR(3)
    h_S = RPAR(4)
    x_E = RPAR(5)
    x_S = RPAR(6)

!     salto_alturas = h_S - h_E
!     salto_poshtal = x_S - x_E

!     th_g = datan2(-salto_poshtal,-salto_alturas)

    d_th_g_dh = 0d0
    d_th_g_dt = 0d0
    d_v_g_dh = (v_S - v_E)/(h_S - h_E)

    derivadaGuiadoVHL(1) = - d_v_g_dh * Y(1) * dcos(Y(2))
    derivadaGuiadoVHL(2) = d_th_g_dt
    derivadaGuiadoVHL(3) = - Y(1) * dcos(Y(2))
    derivadaGuiadoVHL(4) = - Y(1) * dsin(Y(2))

end function derivadaGuiadoVHL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine crear_waypoints_orig (Yi,Yf,num_waypoints,order,waypoints)
    real(8),intent(in)  :: Yi(4), Yf(4)
    integer,intent(in)  :: num_waypoints, order
    real(8),intent(out) :: waypoints(4,num_waypoints)
    integer :: i
    if (order == 1) then
        do i = 1,4
            call linspace(Yi(i),Yf(i),num_waypoints,waypoints(i,:))
        enddo
    else if (order == 2) then
        do i = 1,4
            call pol2space(Yi(i),Yf(i),num_waypoints,waypoints(i,:))
        enddo
    else
        do i = 1,4
            call polnspace(Yi(i),Yf(i),num_waypoints,waypoints(i,:),order)
        enddo
    end if

    ! call pol2space(Yi(3),Yf(3),num_waypoints,waypoints(3,:))
    ! call polnspace(Yi(4),Yf(4),num_waypoints,waypoints(4,:),3)

    ! do i = 2,num_waypoints
    !     waypoints(2,i) = datan2 ( -(waypoints(4,i)-waypoints(4,i-1)) , -(waypoints(3,i)-waypoints(3,i-1)) )
    ! enddo

end subroutine crear_waypoints_orig

! subroutine crear_waypoints (Yi,Yf,Yc,th_red,waypoints)
!     real(8),intent(in)  :: Yi(4), Yf(4), Yc(4), th_red
!     ! integer,intent(in)  :: num_waypoints, order
!     real(8),allocatable,intent(out) :: waypoints(:,:)
    
!     integer :: i,np1,np2

!     np1 = int((dlog(Yc(2)) - dlog(Yi(2))) / (dlog(1-th_red)))
!     write(*,*) np1
!     np2 = int(np1 * Yi(3)/(Yi(3) - Yc(3)))
!     write(*,*) np2

!     allocate(waypoints(size(Yi),np2-1))

!     waypoints(:,1) = Yi
!     call pol2space(Yi(1),Yc(1),np1,waypoints(1,:np1))
!     do i = 2,np1
!         waypoints(2,i) = waypoints(2,i-1) * (1 - th_red)
!     enddo
!     call pol2space(Yi(3),Yc(3),np1,waypoints(3,:np1))
!     do i = 2,np1
!         waypoints(4,i) = waypoints(4,i-1) - dtan(waypoints(2,i-1)) * (waypoints(3,i-1) - waypoints(3,i))
!     enddo

!     ! waypoints(:,np1+1) = Yc(:)
!     call pol2space(Yc(1),Yf(1),np2-np1,waypoints(1,np1:np2))
!     call pol2space(Yc(2),Yf(2),np2-np1,waypoints(2,np1:np2))
!     call pol2space(Yc(3),Yf(3),np2-np1,waypoints(3,np1:np2))
!     call pol2space(Yc(4),Yf(4),np2-np1,waypoints(4,np1:np2))
! end subroutine crear_waypoints

end module guiado
