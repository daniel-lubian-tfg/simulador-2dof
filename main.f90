program sim

use math,       only:   RK4, &
                        PI
use util,       only:   check_waypoint,&
                        Y2X, &
                        comprobar_diana, &
                        arregla_angulo
use guiado,     only:   crear_WP_1tramo,&
                        derivadaGuiadoVHL,&
                        calcularControlGuiadoVHL,&
                        calcularMatrizA,&
                        calcularMatrizB
use control,    only:   calcularMatrizKpKd
use simulador,  only:   derivadaNoLineal, &
                        perturbacionY, &
                        saturadorU

implicit none



!!!!!!! CONFIGURACION SIMULADOR
!       test a simular
INTEGER, PARAMETER :: TEST = 5
!       condiciones iniciales a simular
INTEGER, PARAMETER :: CI = 1
!       saturadores a aplicar
logical, PARAMETER, dimension(2) :: SAT_U_MIN = (/.TRUE. , .TRUE./)
logical, PARAMETER, dimension(2) :: SAT_U_MAX = (/.TRUE. , .TRUE./)
!       recalcular waypoints
LOGICAL, PARAMETER :: RECALCULAR_WP = .TRUE.
! LOGICAL, PARAMETER :: RECALCULAR_WP = .FALSE.
!       si se guardan los datos
logical, parameter :: GUARDAR_DATOS = .TRUE.
! logical, parameter :: GUARDAR_DATOS = .FALSE.
!       realimentacion o no
logical, parameter :: REALIMENTACION = .TRUE.
! logical, parameter :: REALIMENTACION = .FALSE.
!       paso de tiempo
real(8), parameter :: dt = 1d-2
!       numero maximo de pasos de integracion
integer, parameter :: pasos_int = 1d2*int(1d0/dt)
!       condiciones diana
real(8), dimension(4), parameter :: Y_DIANA_MAX = (/ 5d0 ,&
                                                     5d0*PI/180d0,&
                                                     1d-1,&
                                                   1.5d0 /)
real(8), dimension(4), parameter :: Y_DIANA_MIN = (/ 1d-2,&
                                                    -5d0*PI/180d0,&
                                                    -1d-1,&
                                                  -1.5d0 /)
!       rango condiciones iniciales
real(8), dimension(4), parameter :: RYiM = (/ 1D2, -1.5D0, 5D2, -2D3 /)
real(8), dimension(4), parameter :: RYiX = (/ 4D2,  1.5D0, 1D4,  2D3 /)
        ! Rango Y inicial Minimo
        ! Rango Y inicial maXimo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! DECLARACION DE VARIABLES

! VARIABLES WAYPOINS
real(8) :: Y_inicial(4), Y_critico(4), Y_final(4)
integer :: n_wp, n_wp2(2), n_wp2_aux(2), i, ii
real(8), allocatable :: Y_wp0(:,:), Y_wp(:,:)
logical :: paso_waypoint = .FALSE.
LOGICAL :: PASO_WP_CRITICO = .FALSE.

logical :: DENTRO
integer :: dentro_int
REAL(8) :: aux = 1d2
integer, allocatable :: seed(:)
integer :: size_seed, clock, ejecucion

real(8) :: t, VHL_cond(6), t_

real(8), dimension(4) :: Y, Yg, Yp, Yd, Ydg, Y_, Yg_
real(8), dimension(2) :: X, Xg,     Xd, Xdg
real(8), dimension(2) :: U, Ug, Up

real(8), dimension(2,2) :: A, B, KP, KD
real(8) :: polos(2), modos(2,2)

! montecarlo:do ejecucion = 1,int(1e+5) ! si lo desactivas recuerda quitar el enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!! CREAR WAYPOINTS iniciales

select case (CI)
    case (0)
        Y_inicial = (/ 75d0, 0.132552d0, 2d2, 2d1 /)
        Y_critico = (/ 10d0, 0.132552d0, 5d1, 0d0 /)
        Y_final   = (/  1d0,        0d0, 0d0, 0d0 /)
        N_WP2     = (/  int(Y_inicial(3)/100), &
                        int(Y_critico(3)/10d0)/)
    case(1)
        Y_inicial = (/ 150d0, 0.2d0 , 5d2, 3d1 /)
        Y_critico = (/  10d0,   5d-2, 5d1, 0d0 /)
        Y_final   = (/   1d0,   0d0 , 0d0, 0d0 /)
        N_WP2     = (/  int(Y_inicial(3)/100), &
                        int(Y_critico(3)/10d0)/)
        print*, N_WP2
    case(2)
        Y_inicial = (/ 3d2,  0.5d0 , 8d3, 1d3 /)
        Y_critico = (/ 5d1,    1d-3, 5d2, 0d0 /)
        Y_final   = (/ 1d0,   0d0 , 0d0, 0d0 /)
        N_WP2     = (/  int(Y_inicial(3)/1000), &
                        int(Y_critico(3)/10d0)/)
    case(3)
        Y_inicial = (/ 4d2, 5d-1, 8d3, 1d3 /)
        Y_critico = (/ 1d1, 1d-3, 5d1, 0d0 /)
        Y_final   = (/ 1d0, 0d0 , 0d0, 0d0 /)
        N_WP2     = (/  int(Y_inicial(3)/500), &
                        int(Y_critico(3)/5d0)/)
    case(4)
        Y_inicial = (/ 1d3,  5d-1, 8d3, 1d3 /)
        Y_critico = (/ 1d1, 1d-3, 5d1, 0d0 /)
        Y_final   = (/ 1d0,  0d0 , 0d0, 0d0 /)
        N_WP2     = (/  int(Y_inicial(3)/500), &
                        int(Y_critico(3)/5d0)/)
    case (100)
        call RANDOM_SEED(size=size_seed)
        allocate(seed(size_seed))
        call system_clock(count=clock)
        seed = clock + 37 * (/ (i - 1, i = 1, size_seed) /)
        call RANDOM_SEED(put=seed)
        deallocate(seed)
        call RANDOM_NUMBER(Y_inicial)
        Y_inicial(1) = RYiM(1) + (RYiX(1) - RYiM(1))*Y_inicial(1)
        Y_inicial(2) = RYiM(2) + (RYiX(2) - RYiM(2))*Y_inicial(2)
        Y_inicial(3) = RYiM(3) + (RYiX(3) - RYiM(3))*Y_inicial(3)
        Y_inicial(4) = RYiM(4) + (RYiX(4) - RYiM(4))*Y_inicial(4)
!         if (Y_inicial(3) > 2.5d3) then
!             aux = 1d3
!         end if
        Y_critico = (/  5d1,  1d-2, 1d2, 0d0 /)
        Y_final   = (/  1d0,  0d0 , 0d0, 0d0 /)
        N_WP2     = (/  int(Y_inicial(3)/1.1d2)+2, &
                        int(Y_critico(3)/5.5d1)+2 /)
end select

n_wp2_aux = N_WP2

allocate(Y_wp0(size(Y_inicial),n_wp2(1)+n_wp2(2)-1))

call crear_WP_1tramo(   Y_inicial, &
                        Y_critico, &
                        N_WP2(1), &
                        Y_wp0(:,:n_wp2(1)))
                        
call crear_WP_1tramo(   Y_critico, &
                        Y_final, &
                        N_WP2(2), &
                        Y_wp0(:,n_wp2(1):n_wp2(1)+n_wp2(2)-1))

    n_wp = int(size(Y_wp0)/size(Y_inicial))
    allocate(Y_wp(size(Y_inicial),n_wp))
    Y_wp = Y_wp0

! write(*,*) "WP creados"

!! ABRIR ARCHIVOS GUIADO, MAT, SIM
if (GUARDAR_DATOS .eqv. .TRUE.) then
    open(unit=100,file="res/dat/Y_wp0.txt")
    open(unit=101,file="res/dat/Y_wp.txt")
    open(unit=200,file="res/dat/Yg.txt")
    open(unit=201,file="res/dat/Y.txt")
    open(unit=202,file="res/dat/Ydg.txt")
    open(unit=203,file="res/dat/Yd.txt")
!     open(unit=204,file="res/dat/Yp.txt")
    open(unit=300,file="res/dat/A.txt")
    open(unit=301,file="res/dat/B.txt")
    open(unit=302,file="res/dat/KP.txt")
    open(unit=303,file="res/dat/KD.txt")
    open(unit=400,file="res/dat/Ug.txt")
    open(unit=401,file="res/dat/U.txt")
!     open(unit=404,file="res/dat/Up.txt")
end if

!! INICIALIZAR VARIABLES

t       = 0d0
Yp(:)   = 0d0
Up(:)   = 0d0

! write(*,*) "INICIO"
! WRITE(*,*) Y_inicial
! write(*,*) t, .TRUE., Y_wp0(:,1)

do i = 2,n_wp
    ! Recalculo de waypoints
    if (i>2) then
        Y_wp(:,i-1) = Y
        if (RECALCULAR_WP .eqv. .FALSE.) then
            Y_wp(2,i-1) = datan2(   -(Y_wp(4,i)-Y_wp(4,i-1)), &
                                    -(Y_wp(3,i)-Y_wp(3,i-1)))
        else
            if (PASO_WP_CRITICO .eqv. .FALSE. .and. n_wp2_aux(1) > 2) then
                n_wp2_aux = n_wp2_aux - (/1,0/)
                CALL crear_WP_1tramo(   Y_wp(:,i-1), &
                                        Y_critico, &
                                        n_wp2_aux(1), &
                                        Y_wp(:,i-1:N_WP2(1)) &
                                    )
            else if (PASO_WP_CRITICO .eqv. .TRUE. .and. n_wp2_aux(2) > 2) then
                n_wp2_aux = n_wp2_aux - (/0,1/)
                call crear_WP_1tramo(   Y_wp(:,i-1), &
                                        Y_final, &
                                        n_wp2_aux(2), &
                                        Y_wp(:,i-1:n_wp2(1)+n_wp2(2)-1) &
                                    )
            end if
        end if
        
    end if
!                   Y_E         Y_S
    VHL_cond = (/   Y_wp(1,i-1),Y_wp(1,i),&
                    Y_wp(3,i-1),Y_wp(3,i),&
                    Y_wp(4,i-1),Y_wp(4,i) /)

    Yg = Y_wp(:,i-1)
    
    if (i == 2) then
        Y = Yg
    end if

    do ii = 1,pasos_int
    
        t_ = t+dt
        
        
        ! CALCULAR Ug (T_g,d_g)
        call calcularControlGuiadoVHL(Yg,Ug,VHL_cond)
        
        ! CALCULAR MATRICES
        call calcularMatrizA(Yg,Ug,VHL_cond, A)
        call calcularMatrizB(Yg,Ug,VHL_cond, B)
        
        ! CALCULAR GANANCIAS
!         polos = (/ - 7d0, - 3d0 /)
!         polos = (/ - 2d1, - 1d1 /)
        polos = (/ - 13d0 , -8d0 /)
        modos = reshape((/1d0,0d0,0.6d0,0.8d0/),(/2,2/))
        
        call calcularMatrizKpKd(A,B,KP,KD,polos,modos)
           
        ! SIMULADOR
        
        ! TEST
        call perturbacionY(t,Yg,Yp, TEST)
        Y = Y + Yp

        ! CALCULO DERIVADAS
        Ydg = derivadaGuiadoVHL(t, Yg, VHL_cond)
        Yd  = derivadaNoLineal (t,  Y, U)

        ! Y2X
        X   = Y2X(Y)
        Xd  = Y2X(Yd)
        Xg  = Y2X(Yg)
        Xdg = Y2X(Ydg)

        ! REALIMENTACION
        if (REALIMENTACION .eqv. .TRUE.) then
            Up = matmul(KP, X-Xg) + matmul(KD, Xd-Xdg)
        else
            Up(:) = 0d0
        end if
        U = Ug + Up
        
        ! SATURADOR
        call saturadorU(U, SAT_U_MIN, SAT_U_MAX)

        ! INTEGRAR Yg
        call RK4 (  FCN=    derivadaGuiadoVHL,&
                    ti=     t,&
                    tf=     t_,&
                    Yi=     Yg,&
                    Yf=     Yg_,&
                    FPAR=   VHL_cond,&
                    NPASOS= 5)

        ! INTEGRAR Y
        call RK4 (  FCN=    derivadaNoLineal,   &
                    ti=     t,  &
                    tf=     t_, &
                    Yi=     Y,  &
                    Yf=     Y_, &
                    FPAR=   U,  &
                    NPASOS= 5)

        if (GUARDAR_DATOS .eqv. .TRUE.) then
            write(200,*) t, Yg
            write(400,*) t, Ug
            write(300,*) t, A(1,1),  A(1,2),  A(2,1),  A(2,2)
            write(301,*) t, B(1,1),  B(1,2),  B(2,1),  B(2,2)
            write(302,*) t, KP(1,1), KP(1,2), KP(2,1), KP(2,2)
            write(303,*) t, KD(1,1), KD(1,2), KD(2,1), KD(2,2)
            write(201,*) t, Y
            write(202,*) t, Ydg
            write(203,*) t, Yd
            write(401,*) t, U
        end if
        
        ! PARA LA SIGUIENTE ITERACION
        
!         call arregla_angulo(Yg_(2))
!         call arregla_angulo(Y_(2))
        
        Yg = Yg_
        Y  = Y_
        t  = t_
        
        !Comprobar si me voy a pasar el waypoint
        call check_waypoint(Y_wp(:,i),Y,var=3,bool=paso_waypoint)
        call check_waypoint(Y_critico,Y,var=3,bool=PASO_WP_CRITICO)
        if(paso_waypoint .eqv. .TRUE.) then 
!             write(*,*) t, paso_waypoint, (Y(3) - Y_wp(3,i))
!             write(*,*) t, paso_waypoint, Y_wp(:,i)
!             write(*,*) t, paso_waypoint, Y
            paso_waypoint = .FALSE.
            exit
        end if
        
    enddo
    if (GUARDAR_DATOS .eqv. .TRUE.) then
        write(100,*) t, Y_wp0(:,i-1)
        write(101,*) t, Y_wp(:,i-1)
    end if
enddo

if (GUARDAR_DATOS .eqv. .TRUE.) then
    write(100,*) t, Y_wp0(:,n_wp)
    write(101,*) t, Y_wp(:,n_wp)
    close(100)
    close(101)
end if

call comprobar_diana ( Y, Y_DIANA_MIN, Y_DIANA_MAX, DENTRO)

if (DENTRO) then 
    dentro_int = 1
else
    dentro_int = 0
end if

! write(*,*) ""
write(*,*) "SALIDA, t = ", t
!!write(*,*) t
!!write(*,*) Yg
write(*,*) Y
! WRITE(*,*) DENTRO, dentro_int
! WRITE(*,*) ""

open(unit=500,file="res/mc/ci.txt",position="append")
    write(500,*) Y_inicial, dentro_int
    write(*,*) DENTRO, Y_inicial, dentro_int
close(500)
open(unit=501,file="res/mc/sal.txt",position="append")
    write(501,*) t, Y, dentro_int
close(501)

if (GUARDAR_DATOS .eqv. .TRUE.) then
    close(200)
    close(201)
    close(202)
    close(203)
!     close(204)
    close(300)
    close(301)
    close(302)
    close(303)
    close(400)
    close(401)
!     close(404)
end if

deallocate(Y_wp0, Y_wp)

! enddo montecarlo

end program sim

