#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Formateo gráficas:
matplotlib.rcParams['text.latex.unicode'] = True
# plt.rc('font', **{'family':'serif', 'serif':['Latin Modern Roman']})
plt.rc('font', **{'family':'serif', 'serif':['Palatino']})
plt.rc('text', usetex=True)
plt.rc('text.latex',preamble=r'\usepackage{color}\usepackage{siunitx}\usepackage{amsmath}\newcommand{\vect}[1]{\mathrm{\mathbf{#1}}}\newcommand{\mat}[1]{\mathsf{#1}}\usepackage[spanish]{babel}\newcommand{\diff}{\mathrm{d}}')
matplotlib.rcParams.update({'font.size': 15})
extFig = ".pdf"
# extFig = ".png"
dpinum = 200

dirData = "./"
dirData = "res/dat/"

Yg = np.loadtxt(dirData+"Yg.txt")
Ug = np.loadtxt(dirData+"Ug.txt")
A  = np.loadtxt(dirData+"A.txt")
B  = np.loadtxt(dirData+"B.txt")
KP = np.loadtxt(dirData+"KP.txt")
KD = np.loadtxt(dirData+"KD.txt")
Y  = np.loadtxt(dirData+"Y.txt")
U  = np.loadtxt(dirData+"U.txt")
Ydg = np.loadtxt(dirData+"Ydg.txt")
Yd  = np.loadtxt(dirData+"Yd.txt")
# Up = np.loadtxt("res/Up.txt")

Y_wp0 = np.loadtxt(dirData+"Y_wp0.txt")
Y_wp  = np.loadtxt(dirData+"Y_wp.txt")

k = 1
DIR = "res/fig/"

alabel = {  "t":    r"Tiempo, $t$ [\si{\second}]",
            "v":    r"Velocidad, $V$ [\si{\metre\second^{-1}}]",
            "th":   r"Dirección de la velocidad, $\theta$ [\si{\degree}]",
            "h":    r"Altura, $h$ [\si{\metre}]",
            "x":    r"Posición horizontal, $h$ [\si{\metre}]",
            "T":    r"Empuje, $T$ [\si{\kilo\newton}]",
            "d":    r"Dirección del empuje, $\delta$ [\si{\degree}]",
            "a1":    r"Aceleración tangencial, $\dot V$ [\si{\metre\second^{-2}}]",
            "a2":    r"Aceleración normal, $V\dot\theta$ [\si{\metre\second^{-2}}]",
            }

# linea = {   "g":    "C1-",
            # "S":    "C2-"}
linea = {   "g":    "k-",
            "S":    "r-"}

lwg = 2.
lwS = 1.

matplotlib.rcParams.update({'font.size': 10})
plt.figure(k),
plt.plot(Yg[:,4],Yg[:,3],linea["g"], label="Guiado"  ,lw=lwg)
plt.plot( Y[:,4], Y[:,3],linea["S"], label="Simulado",lw=lwS)
plt.scatter(Y_wp0[:,4],Y_wp0[:,3],marker=".",c="k",label="WP originales")#,s=45.)
plt.scatter( Y_wp[:,4], Y_wp[:,3],marker=".",c="r",label="WP recalculados")#,alpha=0.5)
# plt.title("x-h")
plt.xlabel(alabel["x"])
plt.ylabel(alabel["h"])
plt.legend(loc="best")
plt.grid(True)
plt.savefig(DIR+"x-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

matplotlib.rcParams.update({'font.size': 20})
plt.figure(k)
plt.plot(Yg[:,0],Yg[:,1],linea["g"],lw=lwg)
plt.plot( Y[:,0], Y[:,1],linea["S"],lw=lwS)
# plt.scatter(Y_wp0[:,0],Y_wp0[:,1], marker=".")
# plt.scatter( Y_wp[:,0], Y_wp[:,1], marker=".")
# plt.title("t-v")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["v"])
plt.grid(True)
plt.savefig(DIR+"t-v"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],np.rad2deg(Yg[:,2]),linea["g"],lw=lwg)
plt.plot( Y[:,0],np.rad2deg( Y[:,2]),linea["S"],lw=lwS)
# plt.scatter(Y_wp0[:,0],np.rad2deg(Y_wp0[:,2]), marker=".")
# plt.scatter( Y_wp[:,0],np.rad2deg( Y_wp[:,2]), marker=".")
# plt.title("t-th (deg)")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["th"])
plt.grid(True)
plt.savefig(DIR+"t-th"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],Yg[:,3],linea["g"],lw=lwg)
plt.plot( Y[:,0], Y[:,3],linea["S"],lw=lwS)
# plt.scatter(Y_wp0[:,0],Y_wp0[:,3], marker=".")
# plt.scatter( Y_wp[:,0], Y_wp[:,3], marker=".")
# plt.title("t-h")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["h"])
plt.grid(True)
plt.savefig(DIR+"t-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],Yg[:,4],linea["g"],lw=lwg)
plt.plot( Y[:,0], Y[:,4],linea["S"],lw=lwS)
# plt.scatter(Y_wp0[:,0],Y_wp0[:,4], marker=".")
# plt.scatter( Y_wp[:,0], Y_wp[:,4], marker=".")
# plt.title("t-x")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["x"])
plt.grid(True)
plt.savefig(DIR+"t-x"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,1],Yg[:,3],linea["g"],lw=lwg)
plt.plot( Y[:,1], Y[:,3],linea["S"],lw=lwS)
# plt.scatter(Y_wp0[:,1],Y_wp0[:,3], marker=".")
# plt.scatter( Y_wp[:,1], Y_wp[:,3], marker=".")
# plt.title("v-h")
plt.xlabel(alabel["v"])
plt.ylabel(alabel["h"])
plt.grid(True)
plt.savefig(DIR+"v-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(np.rad2deg(Yg[:,2]),Yg[:,3],linea["g"],lw=lwg)
plt.plot(np.rad2deg( Y[:,2]), Y[:,3],linea["S"],lw=lwS)
# plt.scatter(np.rad2deg(Y_wp0[:,2]),Y_wp0[:,3], marker=".")
# plt.scatter(np.rad2deg( Y_wp[:,2]), Y_wp[:,3], marker=".")
# plt.title("th-h")
plt.xlabel(alabel["th"])
plt.ylabel(alabel["h"])
plt.grid(True)
plt.savefig(DIR+"th-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Ydg[:,0],Ydg[:,1],linea["g"],lw=lwg)
plt.plot( Yd[:,0], Yd[:,1],linea["S"],lw=lwS)
# plt.title("t-a1")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["a1"])
plt.grid(True)
plt.savefig(DIR+"t-a1"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Ydg[:,0],np.multiply(Yg[:,1],Ydg[:,2]),linea["g"],lw=lwg)
plt.plot( Yd[:,0],np.multiply( Y[:,1], Yd[:,2]),linea["S"],lw=lwS)
# plt.title("t-a2")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["a2"])
plt.grid(True)
plt.savefig(DIR+"t-a2"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Ug[:,0],Ug[:,1]/1e3,linea["g"],lw=lwg)
plt.plot( U[:,0], U[:,1]/1e3,linea["S"],lw=lwS)
# plt.title("t-T (kN)")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["T"])
plt.grid(True)
plt.savefig(DIR+"t-T"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Ug[:,0],np.rad2deg(Ug[:,2]),linea["g"],lw=lwg)
plt.plot( U[:,0],np.rad2deg( U[:,2]),linea["S"],lw=lwS)
# plt.title("t-d (deg)")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["d"])
plt.grid(True)
plt.savefig(DIR+"t-d"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

###############################################################################

# matplotlib.rcParams.update({'font.size': 18})

plt.figure(k)
plt.plot(A[:,0],A[:,1],"k-")
# plt.title("t-A11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{11}$")
plt.grid(True)
plt.savefig(DIR+"t-A11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(A[:,0],A[:,2],"k-")
# plt.title("t-A12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{12}$")
plt.grid(True)
plt.savefig(DIR+"t-A12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(A[:,0],A[:,3],"k-")
# plt.title("t-A21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{21}$")
plt.grid(True)
plt.savefig(DIR+"t-A21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(A[:,0],A[:,4],"k-")
# plt.title("t-A22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{22}$")
plt.grid(True)
plt.savefig(DIR+"t-A22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,1],"k-")
# plt.title("t-B11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{11}$")
plt.grid(True)
plt.savefig(DIR+"t-B11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,2],"k-")
# plt.title("t-B12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{12}$")
plt.grid(True)
plt.savefig(DIR+"t-B12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,3],"k-")
# plt.title("t-B21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{21}$")
plt.grid(True)
plt.savefig(DIR+"t-B21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,4],"k-")
# plt.title("t-B22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{22}$")
plt.grid(True)
plt.savefig(DIR+"t-B22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,1],"k-")
# plt.title("t-KP11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{11}$")
plt.grid(True)
plt.savefig(DIR+"t-KP11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,2],"k-")
# plt.title("t-KP12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{12}$")
plt.grid(True)
plt.savefig(DIR+"t-KP12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,3],"k-")
# plt.title("t-KP21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{21}$")
plt.grid(True)
plt.savefig(DIR+"t-KP21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,4],"k-")
# plt.title("t-KP22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{22}$")
plt.grid(True)
plt.savefig(DIR+"t-KP22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,1],"k-")
# plt.title("t-KD11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{11}$")
plt.grid(True)
plt.savefig(DIR+"t-KD11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,2],"k-")
# plt.title("t-KD12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{12}$")
plt.grid(True)
plt.savefig(DIR+"t-KD12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,3],"k-")
# plt.title("t-KD21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{21}$")
plt.grid(True)
plt.savefig(DIR+"t-KD21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,4],"k-")
# plt.title("t-KD22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{22}$")
plt.grid(True)
plt.savefig(DIR+"t-KD22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

#plt.show()


