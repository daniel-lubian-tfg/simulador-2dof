module vehiculo

use math,   only: PI

implicit none

save

real(8), parameter ::LONG_LANZADOR = 42d0    !(m)
real(8), parameter ::DIAM_LANZADOR = 3.66d0   !(m)

real(8), parameter :: EMPUJE_SL = 756.d+3  ! N
real(8), parameter :: EMPUJE_vac = 825.d+3 ! N

!  VALORES INVENTADOS
real(8), parameter :: T_MAX = 7e+6  ! N
real(8), parameter :: T_MIN = 0.    ! N
real(8), parameter :: d_MAX = + PI/2D0  ! rad
real(8), parameter :: d_MIN = - PI/2D0  ! rad


! # OJO EN UN FUTURO NO SER�N CONSTANTES:
real(8), parameter ::coef_resistencia = 1d0      ! adim,
real(8), parameter :: C_D = coef_resistencia
real(8), parameter ::masa_primera_etapa = 14d+3 !(410. - 396.)*1.e+3 #(kg)

end module vehiculo
