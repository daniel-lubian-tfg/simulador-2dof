#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os

# Formateo gráficas:
matplotlib.rcParams['text.latex.unicode'] = True
# plt.rc('font', **{'family':'serif', 'serif':['Latin Modern Roman']})
plt.rc('font', **{'family':'serif', 'serif':['Palatino']})
plt.rc('text', usetex=True)
plt.rc('text.latex',preamble=r'\usepackage{color}\usepackage{siunitx}\usepackage{amsmath}\newcommand{\vect}[1]{\mathrm{\mathbf{#1}}}\newcommand{\mat}[1]{\mathsf{#1}}\usepackage[spanish]{babel}\newcommand{\diff}{\mathrm{d}}')
matplotlib.rcParams.update({'font.size': 20})
extFig = ".png"
extFig = ".pdf"
dpinum = 200

# for i in range(0,100000):
    # os.system("./mc_T5")
    
ci = np.loadtxt("res/mc/ci2.txt")

print(ci.shape)


v1  = open("res/mc/v1.txt", "w+",encoding="latin1")
v2  = open("res/mc/v2.txt", "w+",encoding="latin1")
v3  = open("res/mc/v3.txt", "w+",encoding="latin1")
v4  = open("res/mc/v4.txt", "w+",encoding="latin1")
v5  = open("res/mc/v5.txt", "w+",encoding="latin1")
v6  = open("res/mc/v6.txt", "w+",encoding="latin1")
th1 = open("res/mc/th1.txt","w+",encoding="latin1")
th2 = open("res/mc/th2.txt","w+",encoding="latin1")
th3 = open("res/mc/th3.txt","w+",encoding="latin1")
th4 = open("res/mc/th4.txt","w+",encoding="latin1")
th5 = open("res/mc/th5.txt","w+",encoding="latin1")
th6 = open("res/mc/th6.txt","w+",encoding="latin1")

# sacar V
for i in range(ci.shape[0]):
    d = ci[i,0]
    if d >= 99.9 and d < 150.:
        v1.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,0],ci[i,4]))
    elif d >= 150. and d < 200.:
        v2.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,0],ci[i,4]))
    elif d >= 200. and d < 250.:
        v3.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,0],ci[i,4]))
    elif d >= 250. and d < 300.:
        v4.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,0],ci[i,4]))
    elif d >= 300. and d < 350.:
        v5.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,0],ci[i,4]))
    elif d >= 350. and d < 401.:
        v6.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,0],ci[i,4]))
        
    d = ci[i,1]
    if d >= -1.49 and d < -1.:
        th1.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,1],ci[i,4]))
    elif d >= -1. and d < -0.5:
        th2.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,1],ci[i,4]))
    elif d >= -0.5 and d < 0.:
        th3.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,1],ci[i,4]))
    elif d >= 0. and d < 0.5:
        th4.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,1],ci[i,4]))
    elif d >= 0.5 and d < 1.:
        th5.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,1],ci[i,4]))
    elif d >= 1. and d < 1.51:
        th6.write("%f %f %f %i \n" %(ci[i,3],ci[i,2],ci[i,1],ci[i,4]))

v1.close()
v2.close()
v3.close()
v4.close()
v5.close()
v6.close()
th1.close()
th2.close()
th3.close()
th4.close()
th5.close()
th6.close()

print("Tasa de acierto final: ", np.mean(ci[:,4]))

colour = ["k","r"]
colour = ["C0","C1"]
colour = ["C1","g"]
marcad = [".","o"]
marcad = [".","o"]

# tt = open("res/mc/tabla.txt","w+",encoding="latin1")


def figura_mc(F,VAR,RANGO,FIGNAME):
    v1__= np.loadtxt(F)
    print(F+": ", v1__.shape , np.mean(v1__[:,3]))
    if VAR == "V":
        units = r" \si{\metre\second^{-1}}"
        varname = r"M�dulo de la velocidad, $V_i$"
    elif VAR == "th":
        units = r" \si{\radian}"
        varname = r"Direcci�n de la velocidad, $\theta_i$"
    plt.figure()
    for i in range(0, v1__.shape[0]//4):
        ind = int(np.round(v1__[i,3]))
        alf = 0.5 + 0.5*ind
        # print(v1__[i,3],ind)
        plt.scatter(v1__[i,0],v1__[i,1],c=colour[ind],marker=marcad[ind],alpha=alf)
    # plt.title("Condiciones iniciales en $"+VAR+"$, entre "+str(RANGO[0])+" y "+str(RANGO[1])+units)
    # plt.title(varname+", entre "+str(RANGO[0])+" y "+str(RANGO[1])+units)
    plt.xlabel(r"Posici�n horizontal, $x_i$ [\si{\metre}]")
    plt.ylabel(r"Altura, $h_i$ [\si{\metre}]")
    plt.grid(True)
    plt.savefig(FIGNAME,bbox_inches="tight",dpi=dpinum)
    plt.close('all')

figura_mc("res/mc/v1.txt","V",    [100,150]  ,"res/fig/mc/v1"+extFig)
figura_mc("res/mc/v2.txt","V",    [150,200]  ,"res/fig/mc/v2"+extFig)
figura_mc("res/mc/v3.txt","V",    [200,250]  ,"res/fig/mc/v3"+extFig)
figura_mc("res/mc/v4.txt","V",    [250,300]  ,"res/fig/mc/v4"+extFig)
figura_mc("res/mc/v5.txt","V",    [300,350]  ,"res/fig/mc/v5"+extFig)
figura_mc("res/mc/v6.txt","V",    [350,400]  ,"res/fig/mc/v6"+extFig)
figura_mc("res/mc/th1.txt","th",  [-1.5,-1.0],"res/fig/mc/th1"+extFig)
figura_mc("res/mc/th2.txt","th",  [-1.0,-0.5],"res/fig/mc/th2"+extFig)
figura_mc("res/mc/th3.txt","th",  [-0.5, 0.0],"res/fig/mc/th3"+extFig)
figura_mc("res/mc/th4.txt","th",  [ 0.0, 0.5],"res/fig/mc/th4"+extFig)
figura_mc("res/mc/th5.txt","th",  [ 0.5, 1.0],"res/fig/mc/th5"+extFig)
figura_mc("res/mc/th6.txt","th",  [ 1.0, 1.5],"res/fig/mc/th6"+extFig)


lista_archivos = [[
    "res/mc/v1.txt",
    "res/mc/v2.txt",
    "res/mc/v3.txt",
    "res/mc/v4.txt",
    "res/mc/v5.txt",
    "res/mc/v6.txt",],[
    "res/mc/th1.txt",
    "res/mc/th2.txt",
    "res/mc/th3.txt",
    "res/mc/th4.txt",
    "res/mc/th5.txt",
    "res/mc/th6.txt",]
]

rango = {
    "res/mc/v1.txt" : r"$V_i:\;\num{100 }\rightarrow\SI{150 }{\metre\second^{-1}}$",
    "res/mc/v2.txt" : r"$V_i:\;\num{150 }\rightarrow\SI{200 }{\metre\second^{-1}}$",
    "res/mc/v3.txt" : r"$V_i:\;\num{200 }\rightarrow\SI{250 }{\metre\second^{-1}}$",
    "res/mc/v4.txt" : r"$V_i:\;\num{250 }\rightarrow\SI{300 }{\metre\second^{-1}}$",
    "res/mc/v5.txt" : r"$V_i:\;\num{300 }\rightarrow\SI{350 }{\metre\second^{-1}}$",
    "res/mc/v6.txt" : r"$V_i:\;\num{350 }\rightarrow\SI{400 }{\metre\second^{-1}}$",
    "res/mc/th1.txt": r"$\theta_i:\;\num{-1.5}\rightarrow\SI{-1.0}{\radian}$",
    "res/mc/th2.txt": r"$\theta_i:\;\num{-1.0}\rightarrow\SI{-0.5}{\radian}$",
    "res/mc/th3.txt": r"$\theta_i:\;\num{-0.5}\rightarrow\SI{ 0.0}{\radian}$",
    "res/mc/th4.txt": r"$\theta_i:\;\num{ 0.0}\rightarrow\SI{ 0.5}{\radian}$",
    "res/mc/th5.txt": r"$\theta_i:\;\num{ 0.5}\rightarrow\SI{ 1.0}{\radian}$",
    "res/mc/th6.txt": r"$\theta_i:\;\num{ 1.0}\rightarrow\SI{ 1.5}{\radian}$",
}

rangoh = [
r"$\num{10000}\rightarrow\num{8000 }$",
r"$\num{8000 }\rightarrow\num{6000 }$",
r"$\num{6000 }\rightarrow\num{4000 }$",
r"$\num{4000 }\rightarrow\num{2000 }$",
r"$\num{2000 }\rightarrow\num{100 }$",
]

mat = np.zeros((5,4,3,2,6))

# print(lista_archivos[1][2])

alt = np.array([10000.1,8000.,6000.,4000.,2000.,0.])
pos = np.array([-1999.9,-1000.,0.,1000.,2000.])

for a in range(0,2):
    tab = open("tab"+str(a)+".tex","w+",encoding="latin1")
    tab.write(r"\toprule"+"\n")
    tab.write(r"$h_i$\textbackslash$x_i$ [\si{\metre}] & $\num{-2000}\rightarrow\num{-1000}$ & $\num{-1000}\rightarrow\num{0}$ & $\num{0}\rightarrow\num{1000}$ & $\num{1000}\rightarrow\num{2000}$ \\"+"\n")
    tab.write(r"\midrule"+"\n")
    vv = 0
    for archivo in lista_archivos[a]:
        dat = np.loadtxt(archivo)
        print(archivo)
        # print(dat)
        for i in range(0,dat.shape[0]):
            d = dat[i,:]
            x = d[0]
            h = d[1]
            v = d[2]
            e = d[3]
            x,h,v,e = d
            for j in range(0,4):
                if x >= pos[j] and x < pos[j+1]:
                    for k in range(0,5):
                        if h < alt[k] and h >= alt[k+1]:
                            mat[k,j,0,a,vv] += 1
                            mat[k,j,1,a,vv] += e
                            mat[k,j,2,a,vv] = mat[k,j,1,a,vv]/mat[k,j,0,a,vv]
                            # print(mat[k,j,0,a])
        mat[:,:,2,:,vv] *= 100.
        
        print(mat[:,:,0,a,vv])

        tab.write(r"\multicolumn{5}{l}{\tabhead{"+rango[archivo]+r"}} \\"+"\n")
        tab.write(r"\midrule"+"\n")
        for i in range(0,5):
            tab.write(rangoh[i]+" & \\num{%6.2f} & \\num{%6.2f} & \\num{%6.2f} & \\num{%6.2f} \\\ \n" %(  mat[i,0,2,a,vv],mat[i,1,2,a,vv],mat[i,2,2,a,vv],mat[i,3,2,a,vv]))
        tab.write(r"\midrule"+"\n")
        vv +=1
    tab.write(r"\bottomrule\\")
    tab.close()


# print(mat[:,:,0,0])

# pasar a porcentaje

# print(mat[:,:,2,0])

# for a in range(0,2):