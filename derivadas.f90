module derivadas

use math only: PI
use tierra only: atmosisa_rho, modelo_gravedad
use vehiculo only: DIAM_LANZADOR, coef_resistencia => C_D, masa_primera_etapa

implicit none

contains

function oscilador(U,t)
    real(8), intent(in) :: U(:),t
    real(8)	:: oscilador(size(U))

    oscilador(1) = U(2)
    oscilador(2) = -2d0*chi*omega_n*U(2) - omega_n*omega_n*U(1)

end function oscilador

subroutine derivadaGuiadoVHL(N,t,Y,F,RPAR,IPAR)
    real(8), intent(in) :: t, Y(N), RPAR(6)
    integer, intent(inout) :: IPAR
    real(8), intent(out) :: F(N)

    v_E = RPAR(1)
    v_S = RPAR(2)
    h_E = RPAR(3)
    h_S = RPAR(4)
    x_E = RPAR(5)
    x_S = RPAR(6)

    salto_alturas = h_S - h_E
    salto_poshtal = x_S - x_E

    th_g = datan2(-salto_poshtal,-salto_alturas)

    d_th_g_dh = 0d0
    d_th_g_dt = 0d0
    d_v_g_dh = (v_S - v_E)/(h_S - h_E)

    F(1) = - d_v_g_dh * Y(1) * dcos(Y(2))
    F(2) = d_th_g_dt
    F(3) = - Y(1) * dcos(Y(2))
    F(4) = - Y(1) * dsin(Y(2))

end subroutine derivadaGuiadoVHL

subroutine derivadaNoLineal(N,t,Y,F,RPAR,IPAR)
    real(8), intent(in) :: t, Y(N), RPAR(2)
    integer, intent(inout) :: IPAR
    real(8), intent(out) :: F(N)

    real(8) :: rho, g, W, S, C_B

    v  = Y(1)
    th = Y(2)
    h  = Y(3)
    x  = Y(4)
    T  = RPAR(1)
    d  = RPAR(2)

    call atmosisa_rho(h,rho)
    call modelo_gravedad(h,g)

    W = masa_primera_etapa * g
    S = PI * (DIAM_LANZADOR/2d0)**2
    C_B = rho * g * S * C_D / (2d0 * W)

    F(1) = g * dcos(th) - g * T * dcos(th-d)/W - C_B*v*v
    F(2) = g * dsin(th) / v - g * T dsin(th-d)/(W*v)
    F(3) = - v * dcos(th)
    F(4) = - v * dsin(th)

end subroutine derivadaNoLineal

end module derivadas
