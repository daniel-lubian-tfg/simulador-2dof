module simulador

use math,       only:   PI
use vehiculo
use tierra
use util,       only:   separarY,&
                        separarU

implicit none

contains

FUNCTION derivadaNoLineal(t,Y,RPAR)
    REAL(8), INTENT(IN) :: t, Y(:), RPAR(:)
    REAL(8) :: derivadaNoLineal(size(Y)), U(2)
    
    REAL(8) :: v, th, h, x, Te, d
    
    real(8) :: rho, g, W, S, C_B
    
    U = RPAR(:)
    
    CALL separarY(Y,v,th,h,x)
    CALL separarU(U,Te,d)
    
!     print*, t, Y, U
    
    call atmosisa_rho   (h,rho)
    call modelo_gravedad(h,g)

    W = masa_primera_etapa * g
    S = PI * (DIAM_LANZADOR/2d0)**2
    C_B = rho * g * S * C_D / (2d0 * W)
    
    derivadaNoLineal(1) = g * dcos(th)     - g * Te * dcos(th-d) / W   &
                        - C_B * v * v
    derivadaNoLineal(2) = g * dsin(th) / v - g * Te * dsin(th-d) / W / v
    derivadaNoLineal(3) = - v * dcos(th)
    derivadaNoLineal(4) = - v * dsin(th)
    
END FUNCTION derivadaNoLineal

subroutine perturbacionY (t, Yg, Yp, test)
    real(8),intent(in) :: t, Yg(4)
    integer,intent(in) :: test
    real(8),intent(out):: Yp(4)
    
    real(8) :: max_amp(2)
    integer, allocatable :: seed(:)
    integer :: size_seed, i, clock
    
    Yp(:) = 0d0
    select case (test)
    case (0)
        return

    case (1)
        if (t < 1d-3) then
            Yp(1) = 0.05d0*Yg(1)
            Yp(2) = 0.05d0*Yg(2)
            Yp(3) = 0d0
            Yp(4) = 0d0
        end if
        return

    case (2)
        if (t > int(t)-1d-2 .and. t < int(t)+1d-2) then
            Yp(1) = 1d-1*Yg(1)
            Yp(2) = 5d-2*Yg(2)
            Yp(3) = 0d0
            Yp(4) = 0d0
        end if
        return
    
    case (3)
        max_amp(1) = 3d-2*Yg(1)
        max_amp(2) = 3d-2*Yg(2)
        call RANDOM_SEED(size=size_seed)
        allocate(seed(size_seed))
        call system_clock(count=clock)
        seed = clock + 37 * (/ (i - 1, i = 1, size_seed) /)
        call RANDOM_SEED(put=seed)
        deallocate(seed)
        call RANDOM_NUMBER(Yp(1:2))
        
        Yp(1) = max_amp(1)*Yp(1) - max_amp(1)/2d0
        Yp(2) = max_amp(2)*Yp(2) - max_amp(2)/2d0
        Yp(3) = 0d0
        Yp(4) = 0d0
        return
        
    case (4)
            Yp(1) = 1d-2*Yg(1)
            Yp(2) = 1d-2*Yg(2)
            Yp(3) = 0d0
            Yp(4) = 0d0
        return
        
    case (5)
        max_amp(1) = 1d-2*Yg(1)
        max_amp(2) = 1d-2*Yg(2)
        call RANDOM_SEED(size=size_seed)
        allocate(seed(size_seed))
        call system_clock(count=clock)
        seed = clock + 37 * (/ (i - 1, i = 1, size_seed) /)
        call RANDOM_SEED(put=seed)
        deallocate(seed)
        call RANDOM_NUMBER(Yp(1:2))
        
        Yp(1) = max_amp(1)*Yp(1) - max_amp(1)/2d0
        Yp(2) = max_amp(2)*Yp(2) - max_amp(2)/2d0
        Yp(3) = 0d0
        Yp(4) = 0d0
        
        if (t/3d0 > int(t)-1d-2 .and. t/2d0 < int(t)+1d-2) then
            Yp(1) = 5d-2*Yg(1) + Yp(1)
            Yp(2) = 5d-2*Yg(2) + Yp(2)
            Yp(3) = 0d0
            Yp(4) = 0d0
        end if

        return
        

    end select

!     write(*,*) t, Yp
end subroutine perturbacionY

subroutine saturadorU(U, sat_U_min, sat_U_max)
    real(8), intent(inout) :: U(2)
    logical, intent(in)    :: sat_U_max(2), sat_U_min(2)
    
    IF (sat_U_min(1) .eqv. .TRUE. .and. U(1) < T_MIN) then
        U(1) = T_MIN
    end if
    IF (sat_U_max(1) .eqv. .TRUE. .and. U(1) > T_MAX) then
        U(1) = T_MAX
    end if
    IF (sat_U_min(2) .eqv. .TRUE. .and. U(2) < d_MIN) then
        U(2) = d_MIN
    end if
    IF (sat_U_max(2) .eqv. .TRUE. .and. U(2) > d_MAX) then
        U(2) = d_MAX
    end if
    
end subroutine saturadorU

end module simulador
